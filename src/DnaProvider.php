<?php

namespace Jfredon\FakeAnalyticsData;

class DnaProvider
{
    public function generate(?int $tupleNumber = null): array
    {
        if ($tupleNumber === null) {
            $tupleNumber = random_int(25, 100);
        }

        $this->simulateWebServiceLatency(2);
        $dnaTuples = [];

        for ($i = 0; $i < $tupleNumber; $i++) {
            $dnaTuples[] = DnaTupleFactory::createRandomDnaTuple();
        }

        return $dnaTuples;
    }

    private function simulateWebServiceLatency(int $delayInSeconds): void
    {
        sleep($delayInSeconds);
    }
}