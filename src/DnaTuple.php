<?php

namespace Jfredon\FakeAnalyticsData;

class DnaTuple
{
    public string $dna1;
    public string $dna2;

    public function __construct(string $dna1, string $dna2)
    {
        $this->dna1 = $dna1;
        $this->dna2 = $dna2;
    }
}