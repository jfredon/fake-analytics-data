<?php

namespace Jfredon\FakeAnalyticsData;

class DnaTupleFactory
{
    public static function createRandomDnaTuple(): DnaTuple
    {
        $length = random_int(3, 30);
        $alphabet = "ACGT";

        $dna1 = '';
        for ($i = 0; $i < $length; $i++) {
            $dna1 .= $alphabet[random_int(0, strlen($alphabet) - 1)];
        }

        $dna2 = '';
        for ($i = 0; $i < $length; $i++) {
            $dna2 .= $alphabet[random_int(0, strlen($alphabet) - 1)];
        }

        return new DnaTuple($dna1, $dna2);
    }
}