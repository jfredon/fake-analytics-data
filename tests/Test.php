<?php

use Jfredon\FakeAnalyticsData\DnaProvider;
use Jfredon\FakeAnalyticsData\DnaTuple;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    /**
     * @test
     */
    public function should_return_one_DNA_tuple() {
        $dnaProvider = new DnaProvider();
        $tupleNumber = 1;

        $dnaTuples = $dnaProvider->generate($tupleNumber);

        self::assertThat($dnaTuples, self::countOf(1));
        self::assertThat($dnaTuples, self::containsOnlyInstancesOf(DnaTuple::class));
    }

    /**
     * @test
     */
    public function should_return_two_DNA_tuple() {
        $dnaProvider = new DnaProvider();
        $tupleNumber = 2;

        $dnaTuples = $dnaProvider->generate($tupleNumber);

        self::assertThat($dnaTuples, self::countOf(2));
        self::assertThat($dnaTuples, self::containsOnlyInstancesOf(DnaTuple::class));
    }
}
